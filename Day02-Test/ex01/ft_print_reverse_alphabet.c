/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_reverse_alphabet.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yiwang <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/11 20:33:02 by yiwang            #+#    #+#             */
/*   Updated: 2018/02/12 11:34:33 by yiwang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int			ft_putchar(char c)
{
	write (1, &c, 1);
	return 0;
}

void ft_print_reverse_alphabet(void)
{
	char z;

	z = 'z';
	while (z >='a')
	{
		ft_putchar(z);
		z = z - 1 ;
	}
}
int main()
{
	ft_print_reverse_alphabet();
	return 0;
}
