/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_numbers.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yiwang <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/12 11:34:51 by yiwang            #+#    #+#             */
/*   Updated: 2018/02/12 11:41:06 by yiwang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int			ft_putchar(char c)
{
	write (1, &c, 1);
	return 0;
}

void	ft_print_numbers(void)
{
	char num;

	num = '0';
	while (num <='9')
	{
		ft_putchar(num);
		num =num + 1;
	}
}

int main()
{
	ft_print_numbers();
	return 0;
}
