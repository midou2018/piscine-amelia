/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is_negative.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yiwang <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/12 11:41:17 by yiwang            #+#    #+#             */
/*   Updated: 2018/02/12 11:49:23 by yiwang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int			ft_putchar(char c)
{
	write (1, &c, 1);
	return 0;
}

void ft_is_negative(int n)
{
	if (n >=0)
	{
		ft_putchar('P');
	}
	else
	{
		ft_putchar('N');
	}
}

int main()
{
	ft_is_negative(-5);
	return 0;
}
