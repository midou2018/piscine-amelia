/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yiwang <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/12 14:38:20 by yiwang            #+#    #+#             */
/*   Updated: 2018/02/20 15:02:33 by yiwang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int			ft_putchar(char c)
{
	write(1, &c, 1);
	return 0;
}

void	ft_putnbr(int n)
{
	long x;

	x = n;
	if (x < 0)
	{
		ft_putchar('-');
		x = -x;
	}
	if (x < 10)
		ft_putchar(x + '0');
	else
	{
		ft_putnbr(x / 10);
		ft_putnbr(x % 10);
	}
}

int main()
{
	ft_putnbr(-150);
	return 0;
}
