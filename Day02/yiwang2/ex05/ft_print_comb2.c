/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb2.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yiwang <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/26 20:05:27 by yiwang            #+#    #+#             */
/*   Updated: 2018/02/26 20:18:15 by yiwang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_putchar(char c);

void	assign_table(char tab[4])
{
	tab[0] = 0;
	tab[1] = 0;
	tab[2] = 0;
	tab[3] = 1;
}

void	print_comb(char tab[4])
{
	ft_putchar(tab[0] + '0');
	ft_putchar(tab[1] + '0');
	ft_putchar(' ');
	ft_putchar(tab[2] + '0');
	ft_putchar(tab[3] + '0');
}

void	ft_print_comb2(void)
{
	char	tab[4];

	assign_table(tab);
	while (!((tab[0] == 9) && (tab[1] == 9)))
	{
		print_comb(tab);
		if (!((tab[0] == 9) && (tab[1] == 8)))
			ft_putchar(',');
		if (!((tab[0] == 9) && (tab[1] == 8)))
			ft_putchar(' ');
		tab[3]++;
		if ((tab[3] = tab[3] % 10) == 0)
			tab[2] = (tab[2] + 1);
		if (tab[2] == 10)
		{
			tab[2] = tab[0];
			tab[1]++;
			if ((tab[1] = tab[1] % 10) == 0)
				tab[0]++;
			tab[3] = tab[1] + 1;
		}
	}
	ft_putchar('\n');
}
