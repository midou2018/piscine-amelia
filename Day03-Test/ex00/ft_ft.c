/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ft.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yiwang <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/13 18:20:57 by yiwang            #+#    #+#             */
/*   Updated: 2018/02/14 11:37:41 by yiwang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

void ft_ft(int *n)
{
	*n =42;
	printf("%d\n", *n);
}

int main(int argc, const char *argv[])
{
	int a;

	ft_ft(&a);
	return 0;
}
