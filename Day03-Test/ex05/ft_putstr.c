/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yiwang <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/13 15:04:22 by yiwang            #+#    #+#             */
/*   Updated: 2018/02/19 16:56:36 by yiwang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int			ft_putchar (char c) 
{
	write (1, &c, 1);
	return 0;
}

void ft_putstr(char *str)
{
	int i;

	i =0;
	while (str[i]!='\0')
	{
		ft_putchar(str[i]);
		i++;
	}
}

int main()
{
	ft_putstr("hello there");
	return 0;
}
