/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlen.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yiwang <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/14 10:54:25 by yiwang            #+#    #+#             */
/*   Updated: 2018/02/14 11:00:28 by yiwang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int			ft_strlen(char *str)
{
	int i;
	
	i = 0;
	while (str[i]!= '\0')
			i++;
	return (i);
}

int main(int argc, char *argv[])
{
	printf("%d",ft_strlen(argv[1]));
	return 0;
}
