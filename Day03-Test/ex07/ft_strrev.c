/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrev.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yiwang <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/13 15:11:15 by yiwang            #+#    #+#             */
/*   Updated: 2018/02/14 10:51:59 by yiwang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

char	*ft_strrev(char *str)
{
	int			i;
	int			char_count;
	char	*tempt_ptr;
	char	tmp;

	i = 0;
	char_count = 0;
	tempt_ptr = str;
	while (*tempt_ptr != '\0')
	{
		char_count++;
		tempt_ptr++;
	}
	while (char_count > i)
	{
		tmp = *(str + char_count - 1);
		*(str + char_count - 1) = *(str + i);
		*(str + i) = tmp;
		i++;
		char_count--;
	}
	return (str);
}

int main(int argc, char *argv[])
{
	printf("%s\n", ft_strrev(argv[1]));
	return 0;
}
