/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yiwang <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/17 17:23:15 by yiwang            #+#    #+#             */
/*   Updated: 2018/02/28 12:13:04 by yiwang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>

int			ft_atoi(char *str)
{
	int		nb;
	int		neg;

	nb = 0;
	while (*str < 33)
		str++;
	if (*str == '-' || *str == '+')
	{
		if (*str == '-')
			neg = 1;
		str++;
	}
	while (*str >= '0' && *str <= '9' && *str != '\0')
	{
		nb = nb * 10 + *str - '0';
		str++;
	}
	if (neg)
		return (-nb);
	return (nb);
}


int		main(int argc, char **argv)
{
	if (argc != 2)
		return (-1);
	printf("Real atoi: %d\n", atoi(argv[1]));
	printf("My atoi: %d\n", ft_atoi(argv[1]));
	return (0);
}
