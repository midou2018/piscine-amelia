/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_integer_table.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yiwang <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/27 19:29:09 by yiwang            #+#    #+#             */
/*   Updated: 2018/02/27 20:09:52 by yiwang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

void	ft_sort_integer_table(int *tab, int size)
{
	int tmp;
	int i;
	int j;

	i = 0;
	while (i < size - 1)
	{
		if (*(tab + i) > *(tab + i + 1))
		{
			j = i;
			while (*(tab + j) > *(tab + j + 1) && j >= 0)
			{
				tmp = *(tab + j);
				*(tab + j) = *(tab + j + 1);
				*(tab + j + 1) = tmp;
				j--;
			}
		}
		i++;
	}
}

int main(int argc, const char *argv[])
{
	int tab[5];
	int size=5;
	int i;
	tab[0] = 4;
	tab[1] = 11888;
	tab[2] = 5;
	tab[3] = 9;
	tab[4] = 245;
	ft_sort_integer_table(tab, size);
	i = 0;
	while (i <= size - 1)
	{
		printf("%d", i);
		printf(" : ");
		printf("%d", tab[i]);
		printf("\n");
		i++;
	}
	
	return 0;
}
