/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iterative_factorial.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yiwang <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/15 13:56:14 by yiwang            #+#    #+#             */
/*   Updated: 2018/02/15 14:32:41 by yiwang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>

int			ft_iterative_factorial(int nb)
{
	int f;

	f = 1;
	if (nb > 12 || nb < 0)
			return (0);
	else
	{
		while (nb > 0)
		{
			f *=nb;
			nb = nb - 1;
		}
	}
	return (f);
}


int main (int argc, const char *argv[])
{
	printf("%d\n", ft_iterative_factorial(atoi(argv[1])));
	return 0;
}
