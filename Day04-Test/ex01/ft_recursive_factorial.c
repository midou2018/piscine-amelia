/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_recursive_factorial.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yiwang <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/15 14:58:49 by yiwang            #+#    #+#             */
/*   Updated: 2018/02/15 15:07:42 by yiwang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>

int			ft_recursive_factorial(int nb)
{
	if (nb < 0 || nb > 12)
			return 0;
	else if (nb >= 1 && nb <= 12)
			return (nb *=ft_recursive_factorial(nb - 1));
	return (1);
}

int main(int argc, const char *argv[])
{
	printf("%d\n", ft_recursive_factorial(atoi(argv[1])));
	return 0;
}
