/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iterative_power.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yiwang <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/15 15:09:11 by yiwang            #+#    #+#             */
/*   Updated: 2018/02/15 15:47:32 by yiwang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>

int			ft_iterative_power(int nb, int power)
{
	int res;
	if (power < 0)
		return (0);
	else if (power == 0)
		return (1);
	res = nb;
	while (power > 1)
	{
		res *=nb;
		power--;
	}
	return (res);
}

int main(int argc, const char *argv[])
{
	printf("%d\n", ft_iterative_power(atoi(argv[1]),atoi(argv[2])));
	return 0;
}
