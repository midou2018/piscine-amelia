/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_recursive_power.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yiwang <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/15 15:48:53 by yiwang            #+#    #+#             */
/*   Updated: 2018/02/15 15:53:57 by yiwang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>

int			ft_recursive_power(int nb, int power)
{
	if (power < 0)
		return (0);
	else if (power == 0)
		return (1);
	nb = ft_recursive_power(nb, power - 1) *nb;
	return (nb);
}

int main (int argc, const char *argv[])
{
	printf("%d\n",ft_recursive_power(atoi(argv[1]),atoi(argv[2])));
	return 0;
}
