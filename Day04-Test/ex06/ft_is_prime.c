/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is_prime.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yiwang <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/15 16:53:45 by yiwang            #+#    #+#             */
/*   Updated: 2018/02/15 18:06:18 by yiwang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>

int			ft_is_prime(int nb)
{
	int i;

	i = 2;
	while (nb % i != 0)
	{
		if (i > nb)
				break;
		i++;
	}
	if (i == nb)
		return (1);
	else
		return (0);
}

int main(int argc, const char *argv[])
{
	printf("%d\n", ft_is_prime(atoi(argv[1])));
	return 0;
}
