/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_test.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yiwang <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/20 16:24:19 by yiwang            #+#    #+#             */
/*   Updated: 2018/02/21 15:05:46 by yiwang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <unistd.h>

int		ft_putchar(char c)
{
	write(1, &c,1);
	return (0);
}

void	ft_putnbr(int nb)
{
	long x;

	x = nb;
	if (x < 0)
	{
		ft_putchar('-');
		x = -x;
	}
	if (x < 10)
		ft_putchar(x + '0');
	else
	{
		ft_putnbr(x / 10);
		ft_putnbr(x % 10);
	}
}

int main(int argc, char *argv[])
{
	ft_putnbr(atoi(argv[1]));
	return 0;
}
