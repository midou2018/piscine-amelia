/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi_test.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yiwang <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/21 10:34:46 by yiwang            #+#    #+#             */
/*   Updated: 2018/02/28 12:22:44 by yiwang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>

int		ft_atoi(char *str)
{
	int		nb;
	int		neg;

	nb = 0;
	neg = 0;
	while (*str < 33)
		str++;
	if (*str == '-')
	{
		neg = 1;
		str++;
	}
	else if (*str == '+')
		str++;
	while (*str >= '0' && *str <= '9' && *str != '\0')
	{
		nb = nb * 10 + *str - '0';
		str++;
	}
	if (neg)
		return (-nb);
	return (nb);
}

int main(int argc, char *argv[])
{
	printf("My atoi output: %d\n", ft_atoi(argv[1]));
	printf("The system  atoi output: %d\n", atoi(argv[1]));
	return 0;
}
