/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcpy_test.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yiwang <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/20 18:25:34 by yiwang            #+#    #+#             */
/*   Updated: 2018/02/26 14:13:29 by yiwang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <stdio.h>

char		*ft_strcpy(char *dest, char *src)
{
	int i;

	i = 0;
	while(dest[i] != '\0')
	{
		src[i] = dest [i];
		i++;
	}
	src[i] = '\0';
	return (src);
}

int main(int argc, char *argv[])
{
	printf("My strcpy : %s\n", ft_strcpy(argv[1], argv[2]));
	printf("The system strcpy : %s\n", strcpy(argv[1], argv[2]));
	return 0;
}
