/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr_test.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yiwang <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/20 13:23:35 by yiwang            #+#    #+#             */
/*   Updated: 2018/02/22 14:40:12 by yiwang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

char	*ft_strstr(char *str, char *to_find)
{
	char *st;
	char *fd;

	fd = to_find;
	while (*str != '\0')
	{
		st = str;
		fd = to_find;
		while (*str != '\0' && *fd != '\0' && *str == *fd)
		{
			str++;
			fd++;
		}
		if (*fd == '\0')
			return (st);
		str++;
	}
	return (0);
}

int		main(int argc, char *argv[])
{
	printf("%s\n", ft_strstr(argv[1], argv[2]));
	return 0;
}
