/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yiwang <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/20 16:34:20 by yiwang            #+#    #+#             */
/*   Updated: 2018/02/21 16:27:31 by yiwang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

int		ft_strncmp(char *s1, char *s2, unsigned int n)
{
	while (*s1 && *s1 == *s2 && n > 0)
	{
		s1++;
		s2++;
		n--;
	}
	if (n == 0)
		return (0);
	else
		return (*s1 - *s2);
	return (0);
}

int main(int argc, char *argv[])
{
	printf("My strncpy : %d\n", ft_strncmp(argv[1], argv[2], atoi(argv[3])));
	printf("The system strncpy : %d\n", strncmp(argv[1], argv[2], atoi(argv[3])));
	return 0;
}
