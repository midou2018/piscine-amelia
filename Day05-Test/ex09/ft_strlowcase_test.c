/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlowcase_test.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yiwang <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/21 10:30:19 by yiwang            #+#    #+#             */
/*   Updated: 2018/02/21 13:41:46 by yiwang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

char	*ft_strlowcase(char *str)
{
	char *s1;

	s1 = str;
	while (*s1)
	{
		if (*s1 >= 'A' && *s1 <= 'Z')
		{
			*s1 += 32;
		}
		s1++;
	}
	*s1 = '\0';
	return (str);
}

int main(int argc, char *argv[])
{
	printf("%s\n", ft_strlowcase(argv[1]));
	return 0;
}
