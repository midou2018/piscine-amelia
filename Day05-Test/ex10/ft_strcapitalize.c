/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcapitalize.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yiwang <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/22 12:02:14 by yiwang            #+#    #+#             */
/*   Updated: 2018/02/23 11:28:21 by yiwang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int		is_number(char c)
{
	return ((c >= '0') && (c <= '9'));
}

int		is_upercase(char c)
{
	if (c >= 'A' && c<= 'Z')
		return(1);
	return (0);
}

int		is_lowercase(char c)
{
	return ((c >= 'a') && (c <= 'z'));
}

void	ft_tolow(char *str)
{
	int i;

	i = 0;
	while (str[i])
	{
		if (is_upercase(str[i]))
			str[i] += 32;
		i++;
	}
}

char	*ft_strcapitalize(char *str)
{
	int i;

	i = 0;
	ft_tolow(str);
	if (is_lowercase(str[0]))
		str[0] -= ('a' - 'A');
	i = 1;
	while (str[i])
	{
		if (!is_number(str[i - 1]) && !is_upercase(str[i - 1]) &&
				!is_lowercase(str[i - 1]))
			if (is_lowercase(str[i]))
				str[i] -= 'a' - 'A';
		i++;
	}
	return (str);
}
int	main(int argc, char **argv)
{
	printf("%s\n", ft_strcapitalize("salut, comment tu vas ? 42mots quarante-deux; cinquante+et+un"));
	return 0;
}
