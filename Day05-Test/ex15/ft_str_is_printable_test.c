/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_is_printable_test.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yiwang <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/21 14:24:34 by yiwang            #+#    #+#             */
/*   Updated: 2018/02/21 17:44:48 by yiwang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int		ft_str_is_printable(char *str)
{
	if (!*str)
		return (1);
	while (*str)
	{
		if (!(*str >= 32 && *str <= 127))
			return (0);
		str++;
	}
	return (1);
}

int main(int argc, char *argv[])
{
	char x[1];

	x[0] = '\t';
	printf("%d\n", ft_str_is_printable(argv[1]));
	printf("%d\n", ft_str_is_printable(x));
	return 0;
}
