	/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_concat_params_test.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yiwang <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/27 10:41:27 by yiwang            #+#    #+#             */
/*   Updated: 2018/02/27 14:32:02 by yiwang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>

int		count_chars(int argc, char **argv)
{
	int i;
	int j;
	int chars;

	i = 1;
	j = 0;
	chars = 0;
	while (i < argc)
	{
		j = 0;
		while (argv[i][j])
		{
			j++;
			chars++;
		}
		i++;
	}
	return (chars);
}

void	fill_string(int argc, char **argv, char *concat)
{
	int i;
	int j;
	int c;

	i = 1;
	j = 0;
	c = 0;
	while (i < argc)
	{
		j = 0;
		while (argv[i][j] != '\0')
		{
			concat[c] = argv[i][j];
			j++;
			c++;
		}
		concat[c] = '\n';
		c++;
		i++;
	}
	concat[--c] = '\0';
}

char	*ft_concat_params(int argc, char **argv)
{
	char	*concat;
	int		chars;

	chars = count_chars(argc, argv);
	concat = (char*)malloc(sizeof(char) * (chars + argc));
	fill_string(argc, argv, concat);
	return (concat);
}

int main(int argc, char *argv[])
{
	char *str;

	str = ft_concat_params(argc, argv);
	printf("%s\n", str);
	return 0;
}
