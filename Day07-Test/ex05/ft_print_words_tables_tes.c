
#include <stdio.h>

char	**ft_split_whitespaces(char *str);

int		ft_putchar(char c);

void	ft_print_words_tables(char **tab)
{
	int i;
	int j;

	i = 0;
	while (tab[i])
	{
		j = 0;
		while (tab[i][j])
		{
			ft_putchar(tab[i][j]);
			j++;
		}
		ft_putchar('\n');
		tab++;
	}
}

int main(int argc, char *argv[])
{
	ft_print_words_tables(ft_split_whitespaces(argv[1]));
	return 0;
}
